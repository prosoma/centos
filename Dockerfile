FROM scratch
ADD c5-docker.tar.xz /
LABEL name="CentOS Base Image" \
    vendor="CentOS" \
    license="GPLv2"

COPY ./yum.repos.d/ /etc/yum.repos.d/

RUN yum update -y && yum clean all

# Default command
CMD ["/bin/bash"]
